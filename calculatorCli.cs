//member :

// Ilham Pratama (4210181020)
// Ilham Agung Riyadi (4210181023)
// Dicky Dwi Darmawan (4210181028)


using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient {
    public static void Main()
    {
        try {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;
            while (true)
            {
                Console.Write("first number: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("operand: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("second number: ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();
            }
        reader.Close();
        writer.Close();
        client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}